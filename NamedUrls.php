<?php 

declare(strict_types=1);

namespace Komalbarun;
use Komalbarun\StrPy;


class NamedUrls
{
  
    /**
     * Sets url name and url.
     * 
     * @param string $name name of url(permalink)
     * @param string $url  url(permalink)
     */
    public function add(string $name, string $url)
    {   
        $this->urls[$name] = $url;
    }

    /**
     * Fetch url 
     * 
     * @param  string $name name assigned to url
     * @return string url(permalink)
     */
    public function get(string $name): string
    {
        return $this->urls[$name];
    }

    /**
     * Creates a link to use in hrefs.
     * 
     * @param  string $name   Named assigned to URL.
     * @param  array  $values Array of values to replace klein url parameters 
     *                        in hrefs. Values MUST be in same order as
     *                        specified in klein url.
     *                        
     * @return string         URL to use in hrefs.
     */
    public function link(string $name, array $values): string
    {
        $expl = explode('/', $this->urls[$name]);
        $matches = [];

        foreach($expl as $key=>$value)
        {
            $val = new StrPy($value);

            if($val->startswith('['))
            {
                $matches[] = $value;
                unset($expl[$key]);
            }
        }

        if(count($matches) > count($values))
        {
            throw new \Exception("MISSING VALUES FROM ARRAY");
        }
        elseif (count($matches) < count($values)) 
        {
            throw new \Exception("MISSING PARAMETERS IN URL");
        }
        else
        {
            $new_arr = array_merge($expl, $values);
            return join('/', $new_arr);
        }
    }
}

