## NamedUrls
--------
 Url naming for klein router.

## Installation
----------------
 composer require komalbarun\NamedUrls

## Dependencies
----------------
 * komalbarun\StrPy
 * php >= 7.0

## Usage
---------
```php
<?php

require_once './vendor/autoload.php';

use Komalbarun\NamedUrls;

$names = new NamedUrls();

$names->add('user', '/user/[i:user_id]');
$names->get('user');

// Builds a link based on url name and values from array.
// $urls->link($url_name, $values_array);
echo $names->link('user', [10]);
```
